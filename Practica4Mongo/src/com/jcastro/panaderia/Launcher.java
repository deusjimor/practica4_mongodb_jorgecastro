package com.jcastro.panaderia;

import com.jcastro.panaderia.gui.Controlador;
import com.jcastro.panaderia.gui.Modelo;
import com.jcastro.panaderia.gui.Vista;

public class Launcher {
    public static void main(String[] args) {
        new Controlador(new Vista(), new Modelo());
    }
}
