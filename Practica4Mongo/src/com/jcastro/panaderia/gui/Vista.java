package com.jcastro.panaderia.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.jcastro.panaderia.base.Bollos;
import com.jcastro.panaderia.base.Panes;

import javax.swing.*;

public class Vista {
    JFrame frame;
    private JTabbedPane tabbedPane1;
    private JPanel panel1;

    JTextField txtPan;
    JTextField txtTipoDePan;
    JTextField txtPesoPan;
    DatePicker dpFechaProduccionPan;
    JTextField txtBusqueda;
    JButton btnNuevoPan;
    JButton btnModificarPan;
    JButton btnEliminarPan;
    JList<Panes> lstPanes;
    JButton btnRecargar;

    JTextField txtBollo;
    JTextField txtTipoBollo;
    JTextField txtCalorias;
    JRadioButton rbRelleno;
    JTextField txtBuscarBollos;
    JButton btnNuevoBollo;
    JButton btnModificarBollo;
    JButton btnEliminarBollo;
    JButton btnRecargarBollos;
    JList<Bollos> lstBollos;
    JTextField txtRelleno;

    DefaultListModel<Panes> dlmPanes;
    DefaultListModel<Bollos> dlmBollos;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();

        frame.setLocationRelativeTo(null);
        frame.setSize(500, 330);
    }

    private void inicializar() {
        dlmPanes = new DefaultListModel<>();
        lstPanes.setModel(dlmPanes);

        dlmBollos = new DefaultListModel<>();
        lstBollos.setModel(dlmBollos);

    }
}
