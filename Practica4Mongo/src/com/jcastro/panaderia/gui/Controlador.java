package com.jcastro.panaderia.gui;

import com.jcastro.panaderia.base.Bollos;
import com.jcastro.panaderia.base.Panes;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListerSelecctionListeners(this);
        modelo.conectar();
        listarPanes(modelo.getPanes());
        listarBollos(modelo.getBollos());
    }

    private void addListerSelecctionListeners(Controlador controlador) {
        vista.lstPanes.addListSelectionListener(controlador);
        vista.lstBollos.addListSelectionListener(controlador);
    }

    private void addKeyListeners(Controlador controlador) {
        vista.txtBusqueda.addKeyListener(controlador);
        vista.txtBuscarBollos.addKeyListener(controlador);
    }

    private void addActionListeners(Controlador controlador) {
        vista.btnNuevoPan.addActionListener(controlador);
        vista.btnModificarPan.addActionListener(controlador);
        vista.btnEliminarPan.addActionListener(controlador);
        vista.btnRecargar.addActionListener(controlador);

        vista.btnNuevoBollo.addActionListener(controlador);
        vista.btnModificarBollo.addActionListener(controlador);
        vista.btnEliminarBollo.addActionListener(controlador);
        vista.btnRecargarBollos.addActionListener(controlador);
    }

    private void listarPanes(List<Panes> panes) {
        vista.dlmPanes.clear();
        for (Panes pan : panes) {
            vista.dlmPanes.addElement(pan);
        }
    }

    private void listarBollos(List<Bollos> bollos) {
        vista.dlmBollos.clear();
        for (Bollos bollo : bollos) {
            vista.dlmBollos.addElement(bollo);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Panes pan;
        Bollos bollo;
        switch (e.getActionCommand()) {
            case "nuevoPan" : {
                pan = new Panes();
                adquirirCampos(pan);
                modelo.guardarPan(pan);
                listarPanes(modelo.getPanes());
            } break;
            case "modificarPan" : {
                pan = vista.lstPanes.getSelectedValue();
                adquirirCampos(pan);
                modelo.modificarPan(pan);
                listarPanes(modelo.getPanes());
            } break;
            case "eliminarPan" : {
                pan = vista.lstPanes.getSelectedValue();
                modelo.borrarPan(pan);
                listarPanes(modelo.getPanes());
            } break;
            case "recargar" : {
                listarPanes(modelo.getPanes());
            } break;
            case "nuevoBollo" : {
                bollo = new Bollos();
                adquirirCamposBollo(bollo);
                modelo.guardarBollo(bollo);
                listarBollos(modelo.getBollos());
            } break;
            case "modificarBollo" : {
                bollo = vista.lstBollos.getSelectedValue();
                adquirirCamposBollo(bollo);
                modelo.modificarBollo(bollo);
                listarBollos(modelo.getBollos());
            } break;
            case "eliminarBollo" : {
                bollo = vista.lstBollos.getSelectedValue();
                modelo.borrarBollo(bollo);
                listarBollos(modelo.getBollos());
            } break;
            case "recarBollos" : {
                listarBollos(modelo.getBollos());
            } break;
        }
    }

    private void adquirirCampos(Panes pan) {
        pan.setPan(vista.txtPan.getText());
        pan.setTipoPan(vista.txtTipoDePan.getText());
        pan.setPeso(Integer.parseInt(vista.txtPesoPan.getText()));
        pan.setFechaProduccion(vista.dpFechaProduccionPan.getDate());
    }

    private void adquirirCamposBollo(Bollos bollo) {
        bollo.setBollo(vista.txtBollo.getText());
        bollo.setTipoBollo(vista.txtTipoBollo.getText());
        bollo.setCalorias(Integer.parseInt(vista.txtCalorias.getText()));
        bollo.setRelleno(vista.txtRelleno.getText());
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()) {
            if (e.getSource() == vista.lstPanes) {
                Panes pan = vista.lstPanes.getSelectedValue();
                vista.txtPan.setText(pan.getPan());
                vista.txtTipoDePan.setText(pan.getTipoPan());
                vista.txtPesoPan.setText(String.valueOf(pan.getPeso()));
                vista.dpFechaProduccionPan.setDate(pan.getFechaProduccion());
            }
            if (e.getSource() == vista.lstBollos) {
                Bollos bollo = vista.lstBollos.getSelectedValue();
                vista.txtBollo.setText(bollo.getBollo());
                vista.txtTipoBollo.setText(bollo.getTipoBollo());
                vista.txtCalorias.setText(String.valueOf(bollo.getCalorias()));
                vista.txtRelleno.setText(bollo.getRelleno());
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBusqueda) {
            listarPanes(modelo.getPanes(vista.txtBusqueda.getText()));
        }
        if (e.getSource() == vista.txtBuscarBollos) {
            listarBollos(modelo.getBollos(vista.txtBuscarBollos.getText()));
        }
    }
}
