package com.jcastro.panaderia.gui;

import com.jcastro.panaderia.base.Bollos;
import com.jcastro.panaderia.base.Panes;
import com.jcastro.panaderia.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Modelo {
    private final static String COLECCION_BOLLOS = "Bollos";
    private final static String COLECCION_PANES = "Panes";
    private final static String DATABASE = "Panaderia";

    private MongoClient client;
    private MongoDatabase database;
    private MongoCollection collectionPanes;
    private MongoCollection collectionBollos;

    public void conectar() {
        client = new MongoClient();
        database = client.getDatabase(DATABASE);
        collectionPanes = database.getCollection(COLECCION_PANES);
        collectionBollos = database.getCollection(COLECCION_BOLLOS);
    }

    public void desconectar() {
        client.close();
    }

    public void guardarPan(Panes pan) {
        collectionPanes.insertOne(panToDocument(pan));
    }

    public void guardarBollo(Bollos bollo) {
        collectionBollos.insertOne(bolloToDocument(bollo));
    }

    public void modificarPan(Panes pan) {
        collectionPanes.replaceOne(new Document("_id", pan.getId()), panToDocument(pan));
    }

    public void modificarBollo(Bollos bollo) {
        collectionBollos.replaceOne(new Document("_id", bollo.getId()), bolloToDocument(bollo));
    }

    public void borrarPan(Panes pan) {
        collectionPanes.deleteOne(panToDocument(pan));
    }

    public void borrarBollo(Bollos bollo) {
        collectionBollos.deleteOne(bolloToDocument(bollo));
    }

    public List<Panes> getPanes() {
        ArrayList<Panes> list = new ArrayList<>();

        Iterator<Document> it = collectionPanes.find().iterator();
        while (it.hasNext()) {
            list.add(documentToPan(it.next()));
        }
        return list;
    }

    public List<Bollos> getBollos() {
        ArrayList<Bollos> list = new ArrayList<>();

        Iterator<Document> it = collectionBollos.find().iterator();
        while (it.hasNext()) {
            list.add(documentToBollo(it.next()));
        }
        return list;
    }

    public List<Panes> getPanes(String text) {
        ArrayList<Panes> list = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("pan", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("tipoPan", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> it = collectionPanes.find(query).iterator();
        while (it.hasNext()) {
            list.add(documentToPan(it.next()));
        }
        return list;
    }

    public List<Bollos> getBollos(String text) {
        ArrayList<Bollos> list = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("bollo", new Document("$regex", "/*" + text + "/*")));
        listaCriterios.add(new Document("tipoBollo", new Document("$regex", "/*" + text + "/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> it = collectionBollos.find(query).iterator();
        while (it.hasNext()) {
            list.add(documentToBollo(it.next()));
        }
        return list;

    }

    public Document panToDocument(Panes panes) {
        Document document = new Document();
        document.append("pan", panes.getPan());
        document.append("tipoPan", panes.getTipoPan());
        document.append("peso", panes.getPeso());
        document.append("fechaProduccion", Util.formatearFecha(panes.getFechaProduccion()));
        return document;
    }

    public Document bolloToDocument(Bollos bollo) {
        Document document = new Document();
        document.append("bollo", bollo.getBollo());
        document.append("tipoBollo", bollo.getTipoBollo());
        document.append("calorias", bollo.getCalorias());
        document.append("relleno", bollo.getRelleno());
        return document;
    }

    public Panes documentToPan(Document document) {
        Panes panes = new Panes();
        panes.setId(document.getObjectId("_id"));
        panes.setPan(document.getString("pan"));
        panes.setTipoPan(document.getString("tipoPan"));
        panes.setPeso(document.getInteger("peso"));
        panes.setFechaProduccion(Util.parsearFecha(document.getString("fechaProduccion")));
        return panes;
    }

    public Bollos documentToBollo(Document document) {
        Bollos bollo = new Bollos();
        bollo.setId(document.getObjectId("_id"));
        bollo.setBollo(document.getString("bollo"));
        bollo.setTipoBollo(document.getString("tipoBollo"));
        bollo.setCalorias(document.getInteger("calorias"));
        bollo.setRelleno(document.getString("relleno"));
        return bollo;
    }
}
