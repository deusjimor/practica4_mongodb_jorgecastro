package com.jcastro.panaderia.base;

import org.bson.types.ObjectId;

public class Bollos {
    private ObjectId id;
    private String bollo;
    private String tipoBollo;
    private int calorias;
    private String relleno;

    public Bollos() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getBollo() {
        return bollo;
    }

    public void setBollo(String bollo) {
        this.bollo = bollo;
    }

    public String getTipoBollo() {
        return tipoBollo;
    }

    public void setTipoBollo(String tipoBollo) {
        this.tipoBollo = tipoBollo;
    }

    public int getCalorias() {
        return calorias;
    }

    public void setCalorias(int calorias) {
        this.calorias = calorias;
    }

    public String getRelleno() {
        return relleno;
    }

    public void setRelleno(String relleno) {
        this.relleno = relleno;
    }

    @Override
    public String toString() {
        return bollo + " - " + tipoBollo + " - " + calorias + ".cal";
    }
}
