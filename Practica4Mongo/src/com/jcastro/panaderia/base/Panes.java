package com.jcastro.panaderia.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Panes {
    private ObjectId id;
    private String pan;
    private String tipoPan;
    private int peso;
    private LocalDate fechaProduccion;

    public Panes() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getTipoPan() {
        return tipoPan;
    }

    public void setTipoPan(String tipoPan) {
        this.tipoPan = tipoPan;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public LocalDate getFechaProduccion() {
        return fechaProduccion;
    }

    public void setFechaProduccion(LocalDate fechaProduccion) {
        this.fechaProduccion = fechaProduccion;
    }

    @Override
    public String toString() {
        return pan + " - " + tipoPan + " - " + peso + ".gr - " + fechaProduccion;
    }
}
